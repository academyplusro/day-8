package service;

import bean.QuizMaster;

public class QuizMasterService {

	QuizMaster quizMaster;
	
	public void setQuizMaster(QuizMaster quizMaster) {
		this.quizMaster = quizMaster;
		
	}
	
	public String PopThatQuestion() {
		return quizMaster.popQuestion();
	}

}

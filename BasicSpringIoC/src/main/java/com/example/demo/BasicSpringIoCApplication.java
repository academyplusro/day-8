package com.example.demo;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import service.QuizMasterService;

@SpringBootApplication
public class BasicSpringIoCApplication {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
		
		QuizMasterService service = context.getBean(QuizMasterService.class);
		
        System.out.println(service.PopThatQuestion());
        
        context.close();
	}
}

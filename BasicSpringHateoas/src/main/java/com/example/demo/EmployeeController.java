package com.example.demo;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class EmployeeController {

	 private static final String TEMPLATE = "Hello, %s!";

	    @RequestMapping("/employee")
	    public HttpEntity<Employee> employee(
	            @RequestParam(value = "department", required = false, defaultValue = "iOS Developer") String department) {

	        Employee employee = new Employee(String.format(TEMPLATE, department));
	        employee.add(linkTo(methodOn(EmployeeController.class).employee(department)).withSelfRel());

	        return new ResponseEntity<Employee>(employee, HttpStatus.OK);
	    }
}
